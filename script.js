console.log("Hello World");

let scores=[34,55,66]
console.log(scores);


//for loop
for(let i=0;i<scores.length;i++){
    console.log(scores[i]);
}



function greet(name1){
    return "good morning" +name1;
}
let greeting=greet("uma");
console.log(greeting); 




//example
function greet(name1,age){
    return "good morning"+name1+""+age;
}
let name1 = "uma";
let age = 21;
let details = greet(name1,age);
greet();
console.log(details);




//write a function to check whether the given number is even or odd

function evenorodd(num){
    if(num%2==0){
        return "even number"
    }
    return "odd number"
}

console.log(evenorodd(55));


//write a function to print the sum of n values
function sumOfnumber(n){
    for(i=1;i<=n;i++){
        console.log(i);
    }
}
sumOfnumber(10);



//write a function to check whether the given number is negative

function checknumber(num){
    if(num<0){
        return "negative number";
    }
    else if(num>0 & num<50){
        return "between 1 and 50";
    }
    else{
        return "above 50";
    }
}
console.log(checknumber(-1));



//write a function to print the table3
function table(number){
    for(let i=1;i<=10;i++){
        console.log(number+"*"+i+ "="+number*i);
    }
}
table(5);

//array
const numbers=[12,3,6,78,8]
for(let i=0;i<numbers.length;i++){
    console.log(numbers[i]);
}


//sum of values in the given array

const myNums=[1,2,3,4,5]
let sum=0;
myNums.forEach(num => {
    sum+=num;
})
console.log(sum);


//console operations

console.log(Math.max(50,80));
console.log(Math.min(19,78));
console.log(Math.pow(9,3));
console.log(Math.ceil(23.57));
console.log(Math.floor(23.678));
console.log(Math.abs(-45));
console.log(Math.sqrt(4));
console.log(Math.round(57.89));
console.log(Math.random(10));

//find the maximum values in the given array

function findmax(arr){
    let maxnum=0;
    arr.forEach(element => {
       if(element>maxnum){
        maxnum=element;
       } 
    });
    return maxnum;
}
   let arr=[14,17,34,89];
   console.log(findmax(arr));



//find max value using math function

function findmax(arr){
    let maxnum=0;
    arr.forEach(element => {
       if(element>maxnum){
        maxnum=element;
       } 
    });
    return maxnum;
}
   let listelem=[14,17,34,89];
   console.log(Math.max(...listelem));


//find the minimum values in the given array


function findmin(value){
    let minnum=0;
    arr.forEach(element => {
       if(element>minnum){
        minnum=element;
       } 
    });
    return minnum;
}
   let arrayelem=[5,70,65,15];
   console.log(findmin(arrayelem));





   //reverse the number using while loop
   function reversenum(num){
    let reversednum=0;
   while(num!=0){
    reversednum=(reversednum*10)+num%10;
    num=Math.floor(num/10);
   }
   return reversednum
   }
   
   console.log(reversenum(1234));

   //count the number of digits in the given number

   function countOfdigits(num){
    let count=0;
    while(num!=0){
        count++;
        num=Math.floor(num/10);
    }
    return count
   }
   console.log(countOfdigits(1234));


//sum of given digits

function sumOfDigits(num){
    let sum=0;

    while(num!=0){
        sum=sum+num%10;
        num=Math.floor(num/10)
    }
    return sum;
}
console.log(sumOfDigits(123)); 





//add of numbers in the array

arr=[10,20,30,40];
sum=0;
for(i=0;i<arr.length;i++){
    sum+=arr[i];
}
console.log(sum);


//function addition
function addition(){
    let result=30+40;
return result;
}
console.log(addition());

//addition of three umbers using function
function additionOfThreeNum(num1,num2,num3){
    return num1+num2+num3;
}
let result=additionOfThreeNum(15,12,13);
console.log(result);


//function substraction

function substraction(){
    let result=50-30;
    return result;
}
console.log(substraction());






//print the factors of given number

const num=prompt('enter a positive number:');
console.log('the factors of ${num} is:');

for(let i=1;i<=num;i++){
    if(num%i==0){
        console.log(i);
    }
}


//find the factorial number using push method

function findFactors(num){
    const factors=[];
    let count=0

    for(let i=1;i<=num/2;i++){
        if(num%i==0){
            factors.push(i);
            count++;
        }
    }
    return 'the factors are ${factors} and count is ${factors.length}'
}

console.log(findFactors(25));



//push method
let skills =["java","pyhton","css"]

skills.push("bootstrap");
console.log(skills);
//pop method
skills.pop();
skills.pop();
console.log(skills);



//array slice method()

let values=[2,3,5,7,11,13,17];
//create another array by slicing numbers from index 3 to 5
let newArray=numbers.slice(3,5);
console.log(newArray);




//javascript slice()method

let languages = ["JavaScript", "Python", "C", "C++", "Java"];

let new_arr = languages.slice();
console.log(new_arr); // [ 'JavaScript', 'Python', 'C', 'C++', 'Java' ]

let new_arr1 = languages.slice(2);
console.log(new_arr1); // [ 'C', 'C++', 'Java' ]

let new_arr2 = languages.slice(1, 4);
console.log(new_arr2); // [ 'Python', 'C', 'C++' ]


// the number of words in sentence

function numberofWords(sentence){
    const words = sentence.split(" ")

    return `The number of words is ${words.length}`

}

let sentence = "Hello I am learning Javascript"
console.log(numberofWords(sentence))

// sum of the negative values in an given array
const sumNegative = (numbers) => {
    let result = 0;
    for (let i = 0; i < numbers.length; ++i) {
        if (numbers[i] < 0) {
            result += numbers[i];
        }
    }
    return result;
};




const res = sumNegative([-1, -2, -3, 4, 5, 6]);

console.log(res); 




//check whether a word is palindrome or not

function validatePalin(str) {  
  
    // get the total length of the words  
    const len = string.length;  
  
    // Use for loop to divide the words into 2 half  
    for (let i = 0; i < len / 2; i++) {  
  
        // validate the first and last characters are same  
        if (string[i] !== string[len - 1 - i]) {  
            alert( 'It is not a palindrome');  
        }  
    }  
    alert( 'It is a palindrome');  
}  
  
// accept the string or number from the prompt  
const string = prompt('Enter a string or number: ');  
  
const value = validatePalin(string);  
  
console.log(value);


//arrow functions











//higher order functions
//these are 3 types;map;reduce;filter;

//map

const array =[1,2,3,4,5]
let squared_arr=arr.map((num)=>num*num);
console.log(squared_arr);


//filter
const val =[1,2,3,4,5,6,7,8,9,10]
let multiply_arr=arr.map((num)=>num*num);
console.log(squared_arr);
let even_number=arr.filter((num)=>num%2==0);
console.log(even_number);


//reduce

let sumOfElements=arr.reduce((sum,num)=>sum+num,0);
console.log(sumOfElements);